package com.epam.web.rest;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/calculate")
public interface CalculatorREST {

    @GET
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    Response add(@QueryParam("a") Double a, @QueryParam("b") Double b);
    @GET
    @Path("/subtract")
    @Produces(MediaType.APPLICATION_JSON)
    Response subtract(@QueryParam("a") Double a, @QueryParam("b") Double b);

    @GET
    @Path("/multiply")
    @Produces(MediaType.APPLICATION_JSON)
    Response multiply(@QueryParam("a") Double a, @QueryParam("b") Double b);

    @GET
    @Path("/divide")
    @Produces(MediaType.APPLICATION_JSON)
    Response divide(@QueryParam("a") Double a, @QueryParam("b") Double b);

    @GET
    @Path("/percent")
    @Produces("text/plain")
    Response percent(@QueryParam("number") Double number, @QueryParam("percent") Double percent);

    @GET
    @Path("/pow")
    @Produces("text/plain")
    Response pow(@QueryParam("number") Double number, @QueryParam("pow") Integer pow);

//    @GET
//    @Path("/round")
//    @Produces("text/plain")
//    Response round(@QueryParam("number") Double number, @QueryParam("pow") Integer precision);

    @GET
    @Path("/lastResults")
    @Produces("text/plain")
    Response getLastResults(@QueryParam("count") Integer count);

    @GET
    @Path("/lastResult")
    @Produces(MediaType.APPLICATION_JSON)
    Response getLastResult();
}
