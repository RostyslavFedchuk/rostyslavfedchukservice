package com.epam.web.rest;

import com.epam.model.MathAction;
import com.epam.utils.FileManager;
import com.epam.utils.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.Response;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.stream.Collectors;

public class CalculatorRESTService implements CalculatorREST {
    private static final Logger LOGGER = LogManager.getLogger(CalculatorRESTService.class);

    public Response add(Double a, Double b){
        MathAction obj = new MathAction(a,b, round(a + b, 4),"Addition");
        LOGGER.info(String.format("Action: %f + %f = %.4f",a,b,round(a + b, 4)));
        JsonParser.writeToJson(obj);
        return Response.ok().entity(obj).build();
    }

    public Response subtract(Double a, Double b) {
        MathAction obj = new MathAction(a,b, round(a - b, 4),"Subtraction");
        LOGGER.info(String.format("Action: %f - %f = %.4f",a,b,round(a - b, 4)));
        JsonParser.writeToJson(obj);
        return Response.ok().entity(obj).build();
    }

    public Response multiply(Double a, Double b) {
        MathAction obj = new MathAction(a,b, round(a * b, 4),"Multiplication");
        LOGGER.info(String.format("Action: %f * %f = %.4f",a,b,round(a * b, 4)));
        JsonParser.writeToJson(obj);
        return Response.ok().entity(obj).build();
    }

    public Response divide(Double a, Double b) {
        if (b == 0) {
            LOGGER.warn("Incorrect input! You cannot divide at zero!\n");
            return Response.ok().entity(
                    new MathAction(a,b, -1,"Division. "
                    +"Incorrect input! You cannot divide at zero!\n")).build();
        }
        MathAction obj = new MathAction(a,b, round(a / b, 4),"Division");
        LOGGER.info(String.format("Action: %f / %f = %.4f",a,b,round(a / b, 4)));
        JsonParser.writeToJson(obj);
        return Response.ok().entity(obj).build();
    }

    public Response percent(Double number, Double percent) {
        if (percent < 0 || percent > 100) {
            LOGGER.warn("Incorrect input! Percentage must be in range [ 0 ; 100 ]!\n");
            return Response.ok().entity(-1).build();
        }
        String result = String.format("Action: (%f * %f)/100 = %.4f",number,percent,
                round((number * percent) / 100.0, 4));
        LOGGER.info(result);
        return Response.ok().entity(result).build();
    }

    public Response pow(Double number, Integer pow) {
        if (pow < 0) {
            LOGGER.warn("Incorrect input!We can pow the number only into positive integer value!\n");
            return Response.ok().entity("Incorrect input!We can pow the number only into positive integer value!\n").build();
        }
        String result = String.format("Action: %f^%d = %.4f",number,pow,
                round(Math.pow(number, pow), 4));
        LOGGER.info(result);
        return Response.ok().entity(result).build();
    }

    private double round(Double number, Integer precision) {
        if (precision < 0) {
            LOGGER.warn("Wrong unput! Must be integer to round!\n");
            return -1;
        }
        String pattern = "#.";
        for (int i = 0; i < precision; i++) {
            pattern += "#";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        df.setRoundingMode(RoundingMode.HALF_UP);
        return Double.parseDouble(df.format(number));
    }

    @Override
    public Response getLastResults(Integer count) {
        return Response.ok().entity(FileManager.readFromLog().stream()
                .limit(count).collect(Collectors.joining("\n"))).build();
    }

    @Override
    public Response getLastResult() {
        return Response.ok().entity(JsonParser.readFromJson()).build();
    }
}
