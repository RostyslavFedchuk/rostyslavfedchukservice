package com.epam.web.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebService;
import java.math.RoundingMode;
import java.text.DecimalFormat;

@WebService(endpointInterface = "com.epam.web.soap.CalculatorSOAP")
public class CalculatorSOAPService implements CalculatorSOAP {
    private static final Logger LOGGER = LogManager.getLogger(CalculatorSOAP.class);

    @Override
    public double add(double a, double b) {
        return round(a + b, 4);
    }

    public double subtract(double a, double b) {
        return round(a - b, 4);
    }

    public double multiply(double a, double b) {
        return round(a * b, 4);
    }

    public double divide(double a, double b) {
        if (b == 0) {
            LOGGER.warn("Incorrect input! You cannot divide at zero!\n");
            return -1;
        }
        return round(a / b, 4);
    }

    public double percent(double number, double percent) {
        if (percent < 0 || percent > 100) {
            LOGGER.warn("Incorrect input! Percentage must be in range [ 0 ; 100 ]!\n");
            return -1;
        }
        return round((number * percent) / 100.0, 4);
    }

    public double pow(double number, int pow) {
        if (pow < 0) {
            LOGGER.warn("Incorrect input!We can pow the number only into positive integer value!\n");
            return -1;
        }
        return round(Math.pow(number, pow), 4);
    }

    public double round(double number, int precision) {
        String pattern = "#.";
        for (int i = 0; i < precision; i++) {
            pattern += "#";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        df.setRoundingMode(RoundingMode.HALF_UP);
        return Double.parseDouble(df.format(number));
    }

}
