package com.epam.web.soap;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface CalculatorSOAP {

    @WebMethod
    double add(double a, double b);

    @WebMethod
    double subtract(double a, double b);

    @WebMethod
    double multiply(double a, double b);

    @WebMethod
    double divide(double a, double b);

    @WebMethod
    double percent(double number, double percent);

    @WebMethod
    double pow(double number, int pow);

    @WebMethod
    double round(double number, int precision);
}
