package com.epam.utils;

import com.epam.model.MathAction;
import com.epam.web.rest.CalculatorRESTService;
import com.google.gson.Gson;

import java.io.*;

public class JsonParser {

    public static void writeToJson(MathAction obj){
        try {
            FileWriter writer  = new FileWriter("src/main/resources/results.json");
            new Gson().toJson(obj, writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static MathAction readFromJson(){
        try {
            FileReader reader  = new FileReader("src/main/resources/results.json");
            return new Gson().fromJson(reader, MathAction.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return new MathAction();
        }
    }
}
