package com.epam.utils;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Deque;

public class FileManager {

    public static Deque<String> readFromLog(){
        Deque<String> result = new ArrayDeque<>();
        try(BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/logs.log"))){
            String line = "";
            while ((line = reader.readLine())!=null){
                if(line.matches("^(.)+( - Action: )(.)+$")){
                    result.addFirst(line.substring(line.indexOf("Action:")));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
